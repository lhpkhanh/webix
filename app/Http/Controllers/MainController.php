<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
// use Storage;
use Illuminate\Support\Facades\File;
// use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class MainController extends Controller
{
    public function getData(Request $request){

    	$data = DB::table('t_file')->leftjoin('t_sequence', 't_file.id', '=', 't_sequence.file_id')
    								->where('t_sequence.level', 1)
    								->where('t_file.type', 'folder')
    								->select('t_file.id','t_file.value','t_file.type','t_file.date')
    								->get();
		$folders = DB::table('t_file')->leftjoin('t_sequence', 't_file.id', '=', 't_sequence.file_id')
    								->where('t_sequence.level', '>', 1)
    								->where('t_file.type', 'folder')
    								->select('t_file.id','t_file.value','t_file.type','t_file.date','t_sequence.level', 't_sequence.parent_id')
    								->orderBy('t_sequence.level')->orderBy('t_file.date')
    								->get()->toArray();
		$files = DB::table('t_file')->leftjoin('t_sequence', 't_file.id', '=', 't_sequence.file_id')
    								->where('t_file.type', '<>', 'folder')
    								->select('t_file.id','t_file.value','t_file.type','t_file.date','t_file.size','t_file.icon','t_sequence.level', 't_sequence.parent_id')
    								->orderBy('t_sequence.parent_id')->orderBy('t_file.date')
    								->get();
		for ($i=0; $i < count($folders); $i++){
			$arrayFile = array();
			for ($j=0; $j < count($files); $j++){
				if(($files[$j]->parent_id == $folders[$i]->id) && $files[$j]->level == null) {
					array_push($arrayFile, $files[$j]);
				}
			}
			if(!empty($arrayFile)){
				$folders[$i]->data = $arrayFile;
			}
		}
        for ($i=count($folders)-1; $i >= 0; $i--){
            $arrayFolder = array();
            for ($j=count($folders)-1; $j >= 0; $j--){
                if(($folders[$j]->parent_id == $folders[$i]->id) && $folders[$i]->level > 1) {
                    array_push($arrayFolder, $folders[$j]);
                    array_splice($folders, $j, 1);
                }
            }
            if(!empty($arrayFolder)){
                $arrayFolder = collect($arrayFolder)->sortBy('date')->values()->all();
                if(!empty($folders[$i]->data)){
                    $folders[$i]->data = array_merge($folders[$i]->data, $arrayFolder);
                }else{
                    $folders[$i]->data = $arrayFolder;
                }
                $folders[$i]->open = true;
            }
        }
        if(count($folders) > 0){
            $data[0]->data = $folders;
            $data[0]->open = true;
        }
    	return response()->json(['files'=> $data]);
    }

    public function rename(){
        $id = $_POST['id'];
        $value = $_POST['value'];
        $file = DB::table('t_file')->where('id', $id)->get();
        DB::table('t_file')->where('id', $id)->update(['value' => $value]);
        //******** Use if because not save folder, just save file
        if($file[0]->type != 'folder'){
            Storage::disk('public')->move($file[0]->value, $value);
        }
        return;
    }

    public function delete(){
        $id = $_POST['id'];
        $file = DB::table('t_file')->where('id', $id)->get();
        DB::table('t_file')->delete($id);
        DB::table('t_sequence')->where('file_id', $id)->delete();
        //******** Use if because not save folder, just save file
        if($file[0]->type != 'folder'){
            Storage::disk('public')->delete($file[0]->value);
        }
        return;
    }

    public function cut(){
        $id = $_POST['id'];
        $parentId = $_POST['parentId'];
        DB::table('t_sequence')->where('file_id', $id)->update(['parent_id' => $parentId]);
        return;
    }

    public function upload(Request $request){
        $fileName = $request->fileName;
        $newFileName = $this->checkAndCreateNameExists($fileName);
        $check = strcmp($fileName, $newFileName);
        if($check != 0){
            $fileName = $newFileName;
        }
        $file = $request->file('file');
        Storage::disk('public')->put($fileName, file_get_contents($file));
        if($check != 0){
            return response()->json(['msg'=>true, 'newFileName'=>$fileName]);
        }else{
            return response()->json(['msg'=>false]);
        }
    }

    public function copyPasteAndCreateFolder(Request $request){
        $data = array('value' => $request->value,
                        'type' => $request->type,
                        'size' => $request->size,
                        'icon' => $request->icon);
        $newId = DB::table('t_file')->insertGetId($data);
        if($request->type != 'folder'){
            DB::table('t_sequence')->insert(['file_id' => $newId, 'parent_id' => $request->parentId]);
        }else {
            // *************Create Folder************
            // Storage::disk('public')->makeDirectory($path);
            DB::table('t_sequence')->insert(['file_id' => $newId, 'parent_id' => $request->parentId, 'level' => $request->level]);
        }
        return response()->json(['newId'=> $newId]);
    }

    public function checkAndCreateNameExists($fileName){
        $fileExists =  DB::table('t_file')->where('value', 'like', '%' . explode(".",$fileName)[0] . '%')
                                          ->select('id')->get();
        if(count($fileExists) > 0){
            $sequence = count($fileExists) + 1;
            $fileName = explode(".",$fileName)[0].'('.$sequence.').'.array_reverse(explode(".",$fileName))[0];
        }
        return $fileName;
    }
}
