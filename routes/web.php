<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/test', function () {
    return view('test');
});

Route::get('/getdata','MainController@getData');
Route::post('/rename','MainController@rename');
Route::post('/delete','MainController@delete');
Route::get('/copyPasteAndCreateFolder','MainController@copyPasteAndCreateFolder');
Route::post('/cut','MainController@cut');
Route::post('/upload','MainController@upload');