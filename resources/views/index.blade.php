<!doctype html>
<html>
<head>
	<title>Basic init</title>
	<meta  name = "viewport" content = "initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no">

	<script src="codebase/webix/webix.js?v=6.4.5" type="text/javascript"></script>
	<script src="codebase/filemanager.js?v=6.4.5" type="text/javascript"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	<link rel="stylesheet" type="text/css" href="codebase/webix/webix.css?v=6.4.5">
	<link rel="stylesheet" type="text/css" href="codebase/filemanager.css?v=6.4.5">
	<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<script type="text/javascript">
	$(document).ready(function() {

	});
	webix.ready(function(){
		webix.ui({
			view:"filemanager",
			id:"files"
		});
		var data = getData();
		$$("files").parse(data.files);

		/* RENAME */
		$$("files").attachEvent("onAfterEditStop",function(id,state,editor,view){
			rename(id, state.value);
			webix.message("Rename Success!");
  			return true;
		});

		/* DELETE */
		$$("files").attachEvent("onAfterDelete", function(id){
		    del(id);
		    webix.message("Deleted!");
		    return true;
		});

		/* COPY AND NEW FOLDER */
		var copId;
		$$("files").attachEvent("onBeforeAdd", function(id, obj, index){
			// console.table(obj);
			copId = copyPasteAndCreateFolder(obj);
			webix.message("Create Success!");
			return true;
		});

		$$("files").attachEvent("onAfterAdd", function(id, index){
			$$("files").data.changeId(id, copId);
		});

		/* CUT */
		var cutId;
		$$("files").attachEvent("onBeforeMarkCut", function(ids){
			cutId = ids;
			return true;
		});
		$$("files").attachEvent("onBeforePasteFile", function(id){
			if(cutId != undefined){
				if(cutId.length > 1){
					for (var i = 0; i < cutId.length; i++) {
						cut(cutId[i], id);
					}
				}else {
					cut(cutId, id);
				}
	    		webix.message("Move Success!");
			}
    		return true;
		});

		/* UPLOAD */
		$$("files").attachEvent("onBeforeFileUpload",function(file_config){
			// console.table(file_config);
			var type = file_config.type.toLowerCase();
			var fileExtension = ['jpeg', 'jpg', 'png'];
			if($.inArray(type, fileExtension) == -1){
				webix.message("Only jpg, jpeg, png !","debug");
				return false;
			}else{
				var res = upload(file_config);
				if(res.msg){
					file_config.name = res.newFileName;
				}
		    	return true;
			}
		});

	});
	
	function upload(objFile) {
		var result;
		var _token = $('meta[name="csrf-token"]').attr('content');
		var formData = new FormData();  
		formData.append('file', objFile.file);
		formData.append('fileName', objFile.name);
		formData.append('_token', _token);
	    $.ajax({
	      	url: "/upload",
	      	type:'POST',
	      	enctype: "multipart/form-data",
	      	data: formData,
      		async : false,
      		cache : false,
	      	processData : false,
	      	contentType: false,
      		success: function(data) {
      			result = data;
	      	},
	      	error: function(data) {

			}
		});
	    return result;
	}

	function getData() {
		var result;
	    $.ajax({
	        url: '/getdata',
	        type: 'GET',
	        async : false,
	        contentType: 'application/json; charset=utf-8',
	        cache: false,
	        processData: false
	    }).done(function(data) {
	    	result = data;
	    }).fail(function(e) {
	        
	    });
	    return result;
	}

	function rename(id, value) {
		var _token = $('meta[name="csrf-token"]').attr('content');
	    $.ajax({
	        url: '/rename',
	        type: 'POST',
	        data: {
	        	_token:_token,
	        	id: id,
	          	value: value
	        },
	        cache: false,
	    	success: function(data) {
	    		
        	},
        	error: function() {

        	}
	    });
	}

	function cut(id, parentId) {
		var _token = $('meta[name="csrf-token"]').attr('content');
	    $.ajax({
	        url: '/cut',
	        type: 'POST',
	        data: {
	        	_token:_token,
	        	id: id,
	          	parentId: parentId
	        },
	        cache: false,
	    	success: function(data) {
	    		
        	},
        	error: function() {

        	}
	    });
	}

	function del(id) {
		var _token = $('meta[name="csrf-token"]').attr('content');
	    $.ajax({
	        url: '/delete',
	        type: 'POST',
	        data: {
	        	_token:_token,
	        	id: id
	        },
	        cache: false,
	    	success: function(data) {
	    		
        	},
        	error: function() {

        	}
	    });
	}

	function copyPasteAndCreateFolder(obj) {
		var param = '';
		var type = obj.type;
		if(obj.value != null && obj.value != undefined){
			param += 'value=' + obj.value;
		}
		if(obj.type != null && obj.type != undefined){
			param += '&type=' + obj.type;
		}
		if(obj.size != null && obj.size != undefined){
			param += '&size=' + obj.size;
		}
		if(obj.icon != null && obj.icon != undefined){
			param += '&icon=' + obj.icon;
		}
		if(obj.date != null && obj.date != undefined){
			param += '&date=' + obj.date;
		}
		if(obj.$level != null && obj.$level != undefined && type == 'folder'){
			param += '&level=' + obj.$level;
		}
		if(obj.$parent != null && obj.$parent != undefined){
			param += '&parentId=' + obj.$parent;
		}
		var result;
	    $.ajax({
	        url: '/copyPasteAndCreateFolder?' + param,
	        type: 'GET',
	        async : false,
	        contentType: 'application/json; charset=utf-8',
	        cache: false,
	        processData: false
	    	}).done(function(data) {
		    	result = data.newId;
		    }).fail(function(e) {
		        
		    });
	    return result;
	}
</script>
</body>
</html>