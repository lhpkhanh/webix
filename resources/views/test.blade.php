<!doctype html>
<html>
<head>
	<title>Basic init</title>
	<meta  name = "viewport" content = "initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no">

	<script src="codebase/webix/webix.js?v=6.4.5" type="text/javascript"></script>
	<script src="codebase/filemanager.js?v=6.4.5" type="text/javascript"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	<link rel="stylesheet" type="text/css" href="codebase/webix/webix.css?v=6.4.5">
	<link rel="stylesheet" type="text/css" href="codebase/filemanager.css?v=6.4.5">
	<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<script type="text/javascript">
	$(document).ready(function() {

	});
	webix.ready(function(){
		webix.ui({
		    width:500,
		    rows:[
		        {
		            view:"toolbar", elements:[
		                { view:"button", value:"Save", width:70, click:save_row },
						{ view:"button", value:"Delete", width:70, click:delete_row  },
						{ view:"button", value:"Clear Form", width:100,click:() => $$('myform').clear() }
		            ]
		        },
		        {
		            height:120, cols:[
		                {
		                    view:"form", id:"myform",elements:[
		                        { view:"text", name:"title", placeholder:"Title" },  
        						{ view:"text", name:"year", placeholder:"Year" } 
		                    ]
		                },
		                {
		                    view:"list",
		                    id:"mylist", 
		                    template:"#title# - #year#", // which data to show
		                    select:true, //enables selection 
		                    height:400,
		                    data:[
		                        { id:1, title:"The Shawshank Redemption", year:1994 },
		                        { id:2, title:"The Godfather", year:1972 },
		                        { id:3, title:"The Godfather: Part II", year:1974 },
		                        { id:4, title:"The Good, the Bad and the Ugly", year:1966 },
		                        { id:5, title:"My Fair Lady", year:1964 },
		                        { id:6, title:"12 Angry Men", year:1957 }
		                    ]
		                }
		            ]
		        }
		    ]
		});

		function delete_row() {
		    var id = $$("mylist").getSelectedId();
		    if (id)
		        webix.confirm({
		            title: "Delete",
		            text: "Are you sure you want to delete the selected item?",
		            callback: function(result) { 
		                if (result) {
		                    $$("mylist").remove(id);
		                } 
		            }
		        });
		    else
		        webix.message("Select a film first","debug");
		}

		function save_row(){
		    const formData = $$("myform").getValues();
		    if (formData.id)
		        $$("mylist").updateItem(formData.id, formData);
		    else
		        $$("mylist").add(formData);
		}

		$$("mylist").attachEvent("onAfterSelect", function(id){
		    const record = this.getItem(id);
		    /* {"id":1,"title":"The Godfather","year":1972} */
		    $$("myform").setValues(record);
		});

	});
	
</script>
</body>
</html>